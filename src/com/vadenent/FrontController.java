package com.vadenent;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"", "/main"})
public class FrontController extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response) 
			  throws ServletException, IOException {
		
		String nextPage = "FirstPage.jsp";				// Set to the initial web page for the app.
		String cardText = "That's one Big Volcano!";
		
		// Look for the next page requested from a link in one of the jsp's.
		String requestedAction = request.getParameter("requestedAction");
		
		// If no requestedAction was found the getParameter returns NULL; If
		// NULL, we default to FirstPage.jsp.  Otherwise, we set "nextPage"
		// to the page requested by the link in the jsp.
		
		if ((requestedAction != null) && (requestedAction != "")) {
			
			switch(requestedAction) 
			{
				case "GoToWebPage":
					nextPage = request.getParameter("WebPage");
				
				default:
			}
		}

		System.out.printf("%nShow me nextPage: %s%n%n", nextPage);
		
		switch (nextPage) {
			case "FirstPage.jsp":   cardText = "That's one Big Volcano!"; break;
			case "SecondPage.jsp":  cardText = "Looks COLD up there!"; break;
			case "ThirdPage.jsp":   cardText = "We were right!  It IS COLD up there."; break;
			default:				cardText = "Umm.  Something's not right.";
		}
		request.setAttribute("cardText", cardText);

		request.getRequestDispatcher("/WEB-INF/" + nextPage).forward(request, response);
	
	}

	@Override
	protected void doGet (HttpServletRequest request, HttpServletResponse response) 
			  throws ServletException, IOException {
		
		processRequest(request, response) ;
		
	}

	@Override
	protected void doPost (HttpServletRequest request, HttpServletResponse response) 
			  throws ServletException, IOException {
		
		processRequest(request, response) ;
		
	}

}
